/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package tgjj;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {
    @Test
    void must_Be_Positive_Integer() {
        //arrange
        FizzBuzz methodUnderTest = new FizzBuzz();
        int num = 2;

        //act
        String result = methodUnderTest.fizzBuzz(num);

        //assert
        assertEquals("2", result);
      }

    @Test
    void not_a_positive_integer(){
        //arrange
        FizzBuzz methodUnderTest = new FizzBuzz();
        int num = -2;

        //act & assert
        assertThrows(IllegalArgumentException.class,() -> methodUnderTest.fizzBuzz(num));

    }

    @Test
    void input_is_divisible_by_3_and_5(){
        //arrange
        FizzBuzz methodUnderTest = new FizzBuzz();
        int num = 15;

        //act
        String result = methodUnderTest.fizzBuzz(num);

        //assert
        assertEquals("FizzBuzz", result);
    }

    @Test
    void input_is_divisible_by_5_and_not_3(){
        //arrange
        FizzBuzz methodUnderTest = new FizzBuzz();
        int num = 5;

        //act
        String result = methodUnderTest.fizzBuzz(num);

        //assert
        assertEquals("Buzz", result);
    }

    @Test
    void input_is_divisible_by_3_and_not_5(){
        //arrange
        FizzBuzz methodUnderTest = new FizzBuzz();
        int num = 3;

        //act
        String result = methodUnderTest.fizzBuzz(num);

        //assert
        assertEquals("Fizz", result);
    }

}
